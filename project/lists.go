package main

import "strings"

func givemearray(make_list string) []string {
	// tested function
	// give string list
	// return array

	url_notrack := []string{"https://gitlab.com/quidsup/notrack-blocklists/raw/master/notrack-blocklist.txt", "https://gitlab.com/quidsup/notrack-blocklists/raw/master/notrack-malware.txt"}
	url_default := []string{"https://cataract.shark-inter.net/cataract.txt"}
	url_oldvers := []string{"https://cataract.shark-inter.net/ntrk.txt", "https://cataract.shark-inter.net/unweb.txt"}

	var add_item []string

	make_array := strings.Split(make_list, " ")

	for _, item := range make_array {

		if item == "notrack" {
			add_item = append(add_item, url_notrack...)
		} else if item == "default" {
			add_item = append(add_item, url_default...)
		} else if item == "oldvers" {
			add_item = append(add_item, url_oldvers...)
		} else if item != "empty" && item != " " && item != "" {
			add_item = append(add_item, item)
		}
	}

	return add_item
}
