package main

import (
	"bufio"
	"fmt"
	"net/http"
)

func updatenotify() {

	check_version, err := http.Get("https://cataract.shark-inter.net/version.txt")

	if err == nil {

		scanner := bufio.NewScanner(check_version.Body)

		scanner.Split(bufio.ScanLines)

		for scanner.Scan() {

			if scanner.Text() != cversion {

				updatemessage := `
	*******************************
	*   NEW VERSION AVAILABLE !   *
	*******************************
	`
				fmt.Println(updatemessage)
			}
		}

	} else {

		logger("warning", "It's possible the internet connection is not working.")

	}

	defer check_version.Body.Close()

}
