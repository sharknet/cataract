// close the program
// logger message string

package main

import (
	"bufio"
	"fmt"
	"os"
)

func exit() {
	logger("input", "Press enter to exit...")
	bufio.NewReader(os.Stdin).ReadBytes('\n')
	os.Exit(0)
}

func logger(head string, log_message string) {

	//need global variable debugloglevel [on/off]

	var header string

	if head == "debug" {
		header = "[  DEBUG  ]"
	} else if head == "error" {
		header = "[  ERROR  ]"
	} else if head == "warning" {
		header = "[ WARNING ]"
	} else if head == "input" {
		header = "[  INPUT  ]"
	} else if head == "info" {
		header = "[  INFO   ]"
	} else {
		header = "[ MESSAGE ]"
	}

	if debugloglevel == "on" && head == "debug" {

		fmt.Println(header, log_message)

	} else if head != "debug" {

		fmt.Println(header, log_message)

	}

	if head == "error" {
		exit()
	}
}
