package main

import (
	"fmt"
	"os"
)

func contains(targetlist []string, targetdns string) bool {

	for _, a := range targetlist {
		if a == targetdns {
			return true
			break
		}
	}
	return false
}

func appendinlist(addtolist []string, candidateip string) []string {

	//check for duplicates before adding

	check := contains(addtolist, candidateip)

	if check == false {

		addtolist = append(addtolist, candidateip)

	} else {

		logger("debug", "duplicates in list! "+candidateip)

	}

	return addtolist
}

func cataract(iplist []string, ignorelist []string, refresh bool) {

	hostspath := gethostsfile()

	hostsfilelist := hostfilehandler("array", hostspath)

	logger("debug", "Hosts file path "+hostspath)

	hostfilehandler("clean", hostspath)

	var output string
	var prefixip = (ipprefix + "    ")
	var addtolist []string
	var candidatelist []string
	var newline string

	var skiplinks int = 0
	var addlinks int = 0
	var ignlinks int = 0

	output_hostsfile, err := os.OpenFile(hostspath, os.O_APPEND|os.O_WRONLY, 0600)

	if err == nil {

		if len(hostsfilelist) > 0 && refresh == false {

			for _, x := range iplist {

				hostsfile := contains(hostsfilelist, x)

				if hostsfile == false {

					candidatelist = appendinlist(candidatelist, x)

				}

			}

			candidatelist = append(hostsfilelist, candidatelist...)

		} else {

			hostfilehandler("clean", hostspath)

			hostsfilelist = []string{""}

			candidatelist = iplist

		}

		for _, candidateip := range candidatelist {

			rm_duplicates := contains(addtolist, candidateip)

			if rm_duplicates == false {

				if len(ignorelist) > 0 {

					ignoredns := contains(ignorelist, candidateip)

					if ignoredns == false {

						alreadyinhosts := contains(hostsfilelist, candidateip)

						logger("debug", candidateip)

						if alreadyinhosts == false {

							addlinks += 1

						} else {

							skiplinks += 1
						}

						addtolist = appendinlist(addtolist, candidateip)

					} else {

						ignlinks += 1

					}

				} else {

					alreadyinhosts := contains(hostsfilelist, candidateip)

					logger("debug", candidateip)

					if alreadyinhosts == false {

						addlinks += 1

					} else {

						skiplinks += 1

					}

					addtolist = appendinlist(addtolist, candidateip)

				}

			}

		}

	} else {

		logger("error", "Hosts file is not exist!")

	}

	//windows have different end of line tag, lines bellow will handle that

	if osname == "win" {

		newline = "\r\n"

	} else {

		newline = "\n"

	}

	logger("debug", "cataract line not existing.")

	tagcommet := " #managed_by_cataract"

	for _, output_links := range addtolist {

		if output_links != " " {

			output += newline + prefixip + output_links + tagcommet

		}

	}

	output_hostsfile.WriteString(output)

	string_skiplinks := fmt.Sprintf("%v", skiplinks)
	strint_addlinks := fmt.Sprintf("%v", addlinks)
	string_ignlinks := fmt.Sprintf("%v", ignlinks)

	logger("info", string_skiplinks+" not changed in hosts file.")
	logger("info", strint_addlinks+" are added.")
	logger("info", string_ignlinks+" are excluded.")
	logger("info", "Done!")

	output_hostsfile.Close()

}
