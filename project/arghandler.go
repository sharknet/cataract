package main

import (
	"flag"
)

func arghandler() {

	arg_import := flag.String("import", "empty", "import dna name or list (online/offline)")
	arg_ignore := flag.String("ignore", "ignore-list.txt", "apply ignore list")
	arg_add := flag.String("add", "empty", "add dns name")
	arg_remove := flag.String("remove", "empty", "remove dns name")
	arg_refresh := flag.Bool("refresh", false, "refresh the list")
	arg_destroy := flag.Bool("destroy", false, "remove cataract managed lines")
	arg_help := flag.Bool("help", false, "shwow help")
	arg_prefixip := flag.String("prefixip", "0.0.0.0", "blocking ip addres")

	flag.Parse()

	ipprefix = *arg_prefixip

	if *arg_help == true {
		help()

	} else if *arg_destroy == true {

		hostspath := gethostsfile()

		hostfilehandler("clean", hostspath)

		logger("info", "Cataract linse are removed.")

	} else if *arg_import != "empty" {

		import_to_cataract = listhandler(import_to_cataract, *arg_import)

		ignore_to_cataract = listhandler(ignore_to_cataract, *arg_ignore)

		import_to_cataract = listargumenthandler(import_to_cataract, *arg_add)

		ignore_to_cataract = listargumenthandler(ignore_to_cataract, *arg_remove)

		cataract(import_to_cataract, ignore_to_cataract, *arg_refresh)

	} else if *arg_import == "empty" {

		import_to_cataract = listhandler(import_to_cataract, "notrack default")

		ignore_to_cataract = listhandler(ignore_to_cataract, *arg_ignore)

		import_to_cataract = listargumenthandler(import_to_cataract, *arg_add)

		ignore_to_cataract = listargumenthandler(ignore_to_cataract, *arg_remove)

		cataract(import_to_cataract, ignore_to_cataract, *arg_refresh)

	} else {

		help()

	}

	exit()

}

func listhandler(appendto []string, importlist string) []string {

	//get argument lists, load content to array

	var forward []string

	import_list := givemearray(importlist)

	for _, lists := range import_list {

		addtolist := urlfiletoarray(lists)

		if len(addtolist) > 0 {

			if addtolist[0] != "empty" {

				forward = append(forward, addtolist...)

			}

		}
	}

	forward = append(appendto, forward...)

	return forward

}

func listargumenthandler(appendto []string, importlist string) []string {

	var forward []string

	import_list := givemearray(importlist)

	for _, lists := range import_list {

		validate := urlvalidator(lists)

		if validate != "" {

			forward = append(forward, validate)

		}

	}

	forward = append(appendto, forward...)

	return forward

}

func cataractdestroy() {

	hostspath := gethostsfile()

	hostfilehandler("clean", hostspath)

}
