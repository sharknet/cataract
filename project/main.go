package main

import (
	"fmt"
)

var cversion = "0.4.2"
var ipprefix string
var debugloglevel = "off"
var osname string
var import_to_cataract []string
var ignore_to_cataract []string

func main() {

	banner := `
                        $$$$                    
                   $$$$      $$$$               
              $$$$                $$$$          
          $$$$           $$           $$$$      
       $$$$             $$$$             $$$$   
    $$$$               $$$$$$               $$$$
       $$$$             $$$$             $$$$   
          $$$$           $$           $$$$      
              $$$$                $$$$          
                   $$$$      $$$$               
                        $$$$                    
    
 ####    ##   #####   ##   #####    ##    ####  #####  
#    #  #  #    #    #  #  #    #  #  #  #    #   #    
#      #    #   #   #    # #    # #    # #        #    
#      ######   #   ###### #####  ###### #        #    
#    # #    #   #   #    # #   #  #    # #    #   #    
 ####  #    #   #   #    # #    # #    #  ####    #    
                                                       
Project started: 01.12.2018                  by sharky
Version ` + cversion + `         there is no place like 127.0.0.1
Lists are based on no-track by quidsup

add option --help to see additional options.
`
	fmt.Println(banner)
	updatenotify() //checking for new version and internet connection
	arghandler()

}
