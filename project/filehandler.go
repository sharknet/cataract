// urlfiletoarray "a.txt b.txt" [] string file content

package main

import (
	"bufio"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"strings"
)

func urlfiletoarray(make_list string) []string {
	// tested function
	// load string list from url or file
	// return array

	var format string
	var add_item []string

	check_input, _ := regexp.MatchString(`^http.*:\/\/.*`, make_list)

	if check_input == true {
		format = "url"
	} else {
		format = "file"
	}

	if format == "file" {

		import_list, err := os.Open(make_list)

		if err == nil {

			scanner := bufio.NewScanner(import_list)

			scanner.Split(bufio.ScanLines)

			for scanner.Scan() {

				if strings.HasPrefix(scanner.Text(), "#") == false && strings.HasPrefix(scanner.Text(), " ") == false {

					matches := urlvalidator(scanner.Text())

					if matches != "" {

						logger("debug", matches+" will be added.")

						add_item = append(add_item, matches)
					}
				}
			}

			defer import_list.Close()

		} else {

			logger("warning", "File "+make_list+" can not be loaded, skipping...")

		}

	} else if format == "url" {

		import_list, err := http.Get(make_list)

		if err == nil {

			scanner := bufio.NewScanner(import_list.Body)

			scanner.Split(bufio.ScanLines)

			for scanner.Scan() {

				if strings.HasPrefix(scanner.Text(), "#") == false && strings.HasPrefix(scanner.Text(), " ") == false {

					matches := urlvalidator(scanner.Text())

					if matches != "" {

						logger("debug", matches+" will be added.")

						add_item = append(add_item, matches)
					}
				}
			}

			logger("info", "URL is OK "+make_list)

			defer import_list.Body.Close()

		} else {

			logger("warning", "URL "+make_list+" is not reachable, list will be skipped.")

		}

	} else {

		logger("warning", "Unexpected format in urlfiletoarray function.")

	}

	return add_item
}

func hostfilehandler(mode string, hostfile string) []string {

	var status []string
	var rmcatline []string

	var treimprefix = ipprefix + "    "

	input, err := ioutil.ReadFile(hostfile)
	if err != nil {
		logger("error", "Can read the hosts file.")
	}

	lines := strings.Split(string(input), "\n")

	for i, line := range lines {

		if strings.Contains(line, "#managed_by_cataract") == true {

			if mode == "array" {

				var trimmed string

				trimmed = strings.TrimPrefix(lines[i], treimprefix)
				trimmed = strings.Split(trimmed, " #managed_by_cataract")[0]

				status = appendinlist(status, trimmed)

			}

		} else {
			//remove cataract line
			if mode == "clean" {
				rmcatline = append(rmcatline, lines[i])
			}

		}
	}

	if mode == "clean" {
		output := strings.Join(rmcatline, "\n")
		err = ioutil.WriteFile(hostfile, []byte(output), 0600)
		if err != nil {
			logger("error", "Can not write in to hosts file!")
		}

	}

	return status
}

func urlvalidator(checktheurl string) string {

	r := regexp.MustCompile(`(([A-Z-a-z-0-9]{1,}\.){1,}([a-z-A-Z?])[A-Z-a-z-0-9]{1,})`)
	matches := r.FindString(checktheurl)

	if matches == "" {
		logger("debug", checktheurl+" is not valid, skipping...")
	}

	return matches

}
