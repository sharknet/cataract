package main

import (
	"fmt"
)

func help() {
	fmt.Println("[   HELP   ] [command] [option] [input]                                 [exmplanation]")
	fmt.Println("              command                                                   - set fault list")
	fmt.Println("              command  --import /path/to/the/file.txt                   - add your list to hosts list")
	fmt.Println("              command  --import https://somewebiste.net/list.txt        - add remote list to hosts list")
	fmt.Println("                       if you using mutiple links of files under the apostrophes")
	fmt.Println("                       example:")
	fmt.Println("                       --import '/flie1.txt file2.txt https://www.yxz/file.txt'")
	fmt.Println("              command  --import option                                  - ches preset options")
	fmt.Println("                       preset options:")
	fmt.Println("                       default - cataract + notrack")
	fmt.Println("                       notrack - use only quidsup lists")
	fmt.Println("                       oldvers - cataract + old versions of notrack")
	fmt.Println("              command  --ignore /path/to/the/file.txt                   - ignore list")
	fmt.Println("                              (./ignore-list.txt is default)")
	fmt.Println("              command  --import / --add 'website1.com website2.com'     - add website on the list")
	fmt.Println("              command  --import / --remove 'website1.com website2.com'  - remove website from the list")
	fmt.Println("              command  --import / --refresh                             - clean all cataract managed lines")
	fmt.Println("        main  command  --destroy                                        - remove all sites from hosts file")
	fmt.Println("        main  command  --help                                         	  - show this message")
	fmt.Println("")
	fmt.Println("              command  --prefixip (deafult 0.0.0.0) x.x.x.x             - change prefix ip")
	fmt.Println("                        if you used cataract before, use option --refresh to clean old prefix")
	fmt.Println("")
	fmt.Println("[   INFO   ] create ignore-list.txt in same folder with cataract file and add sites you want to exclude.")
	fmt.Println("[   INFO   ] ignore-list.txt content example:")
	fmt.Println("             website1.com ")
	fmt.Println("             website2.com ")
}
