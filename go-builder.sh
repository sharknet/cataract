#/bin/bash
#cataratc builder

os=$1
arch=$2
out=$3
builddir="../build"

function help {

  echo "[ HELP ] Cataract builder help:"
  echo "         Go have to be installed on you PC to be possible to build the file."
  echo "         This script have to be alongside catartact.go script"
  echo ""
  echo "         you can define arguments while running this script:"
  echo "         go-builder.sh [os name] [architecture] ./cataract.go (already defined)"
  echo ""
  echo "         | OS       | Architecture |"
  echo "         |-------------------------|"
  echo "         | windows  | 386          |"
  echo "         | windows  | amd64        |"
  echo "         | linux    | 386          |"
  echo "         | linux    | amd64        |"
  echo "         | linux    | arm          |"
  echo "         | linux    | arm64        |"
  echo "         | linux    | ppc64        |"
  echo "         | linux    | ppc64le      |"
  echo "         | linux    | mips         |"
  echo "         | linux    | mipsle       |"
  echo "         | linux    | mips64       |"
  echo "         | linux    | mips64le     |"
  echo "         |-------------------------|"
  echo "         | default  | (blank)      |" 
  echo ""
  echo "[ INFO ] default command will create windows and linux 386 and amd64 files. (work as first argument)"
  echo ""
  echo "[ INPUT ] Chose OS for which you want to build the script:"
  read os
  echo "[ INPUT ] Chose the architecture: (if you use 'default' option just press enter)"
  read arch
  echo "[ INPUT ] Chose build file name:"
  read out
  echo "[ INPUT ] Do you want checksum [Y/n]"
  read csum
  main
} 

function main {

  if [[ $os == "windows" ]] && [[ $out != "" ]] ; then 
    if [[ $arch == "386" ]] || [[ $arch == "amd64" ]] ; then
      gobuild
    else
      help
    fi
  elif [[ $os == "linux" ]] && [[ $out != "" ]] ; then
    if [[ $arch == "386" ]] || [[ $arch == "amd64" ]] || [[ $arch == "arm" ]] || [[ $arch == "arm64" ]] || [[ $arch == "ppc64" ]] || [[ $arch == "ppc64" ]] || [[ $arch == "ppc64le" ]] || [[ $arch == "mips" ]] || [[ $arch == "mipsle" ]] || [[ $arch == "mips64" ]] || [[ $arch == "mips64le" ]] ; then
      gobuild
    else
      help
    fi
  
  elif [[ $os == "default" ]] ; then
  
    oss="windows linux"
    archs="386 amd64"
    
    echo "[ INPUT ] Version:"
    echo "[ INFO ] Leave blank (press enter) if you don't want to add the version tag."
    read ver
    if [[ $ver != "" ]] ; then
      version="-v${ver}"
    fi
    
    if [[ $out == "" ]] ; then
      fname="cataract"
      
      else
        fname=$out
    fi
    if [[ $csum != "n" ]] ; then
      echo "[ INPUT ] Do you want checksum [Y/n]"
      read csum
    fi
  
    for os in $oss ; do
      for arch in $archs ; do
        if [[ $os == "windows" ]] && [[ $arch == "386" ]] ; then
          out="${fname}${version}-32bit.exe"
          gobuild
        elif [[ $os == "windows" ]] && [[ $arch == "amd64" ]] ; then
          out="${fname}${version}-64bit.exe"
          gobuild
        elif [[ $os == "linux" ]] && [[ $arch == "386" ]] ; then
          out="${fname}${version}-32bit"
          gobuild
        elif [[ $os == "linux" ]] && [[ $arch == "amd64" ]] ; then
          out="${fname}${version}-64bit"
          gobuild
        else
          echo "[ ERROR ] fast command is not configured properly!"
        
        fi
        
      done
    done
  
  else
    help
  fi

}

function gobuild {
  echo "[ INFO ] Building the file..."
  
  if [ ! -d ${builddir} ] ; then
    mkdir ${builddir}
  fi
  
  env GOOS=${os} GOARCH=${arch} go build -o ${builddir}/${out} project/*.go
  if [[ $csum == "Y" ]] ; then
    md5sum ${builddir}/${out}
    sha256sum ${builddir}/${out}
  fi

  echo "[ INFO ] Done."
}

main
