# cataract

What is hosts file?
The computer file hosts is an operating system file that maps hostnames to IP addresses. It is a plain text file.
The hosts file is one of several system facilities that assists in addressing network nodes in a computer network. 
It is a common part of an operating system's Internet Protocol (IP) implementation, and serves the function of translating 
human-friendly hostnames into numeric protocol addresses, called IP addresses, that identify and locate a host in an IP network. 
Text copied from Wikipedia.

This script will associate scum and tracker domain name to 0.0.0.0 IP address so this websites will not be reachable from your PC, so if some website try to call them they will be blocked.

Script is based on 3 list that are hosted on my website. Lists are based on quidsup NoTrack project + catartacta list.
List can be checked on following links:
`https://cataract.shark-inter.net/ntrk.txt clone of quidsup blocklist`
`https://cataract.shark-inter.net/unweb.txt clone of quidsup malware list`
`https://cataract.shark-inter.net/cataract.txt`

notrack links:
`https://gitlab.com/quidsup/notrack-blocklists/raw/master/notrack-blocklist.txt`
`https://gitlab.com/quidsup/notrack-blocklists/raw/master/notrack-malware.txt`

By default original quidsup lists will be used plus cataract list

More lists will be included in the future.

This tool is **NOT** enough for your protection, it's just additional protection layer.

Please note, if you are using https dns in you browser hosts file will not apply on that requests.
In this case you need to handle blocking on browser level.

You can build this script using go-builder.sh.
This software is made as an experiment with GOlang language, it's created for users that are NOT computer geeks and want to apply some protection by one click.


**Script is NOT tested on MAC.**

GUI application is in progress.

In Windows you can use bat file to run exe file in command line.
In Linux run file from terminal.

**[ Warning ] cataract versions above 0.4.x are not compatible with older versions, please clean yourhosts file if you used cataract before.**

If you want to test, you can create test file that will be used automatically
Windows: C:\temp\testing-hosts
Linux: /tmp/testing-hosts

if this file exist, that will be used isntead original hosts file.